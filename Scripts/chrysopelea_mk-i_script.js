this.name           = "Chrysopelea MK-I";
this.author         = "Tsoj";
this.copyright      = "(C) 2021 Tsoj";
this.licence        = "CC-NC-BY-SA 4.0";
this.description    = "Discounts for Chrysopelea MK-I when at Erlage";
this.version        = "1.0";

"use strict";

this.guiScreenWillChange = function(to, from)
{
	const station = player.ship.dockedStation

	if(
		system.ID == 79 && galaxyNumber == 0 && // Erlage in Galaxy 1
		(to == "GUI_SCREEN_SHIPYARD" || guiScreen == "GUI_SCREEN_SHIPYARD") &&
		station != null && station.hasShipyard
	)
	{
		for(i in Ship.keysForRole("player")){
			const shipKey = Ship.keysForRole("player")[i]
			const shipData = Ship.shipDataForKey(shipKey);
			if(shipData.name.indexOf("hrysopelea") != -1){
				var found = false
				for(j in station.shipyard){
					if(station.shipyard[j].shipdata_key == shipKey){
						found = true;
						break;
					}
				}
				if(found)
					continue;

				const price = shipData._oo_shipyard.price * 0.7
				station.addShipToShipyard({
					short_description:
						shipData.name + ": Exquisite customer model using Erlageian tree wood.\n" +
						"Buying directly from the shipyards of the Design Bureau for Advanced and Fulfilling Technology at Erlage, " +
						"this Chrysopelea will have a decent reduction in price, as well as some extras!", 
					shipdata_key: shipKey, 
					price: price,
					personality: 0, 
					extras: [
						"EQ_WEAPON_BEAM_LASER",
						"EQ_ADVANCED_COMPASS",
						"EQ_ECM",
						"EQ_FUEL_INJECTION",
						"EQ_FUEL_SCOOPS",
						"EQ_SCANNER_SHOW_MISSILE_TARGET",
						"EQ_HARDENED_MISSILE"
					]
				})
			}
		}
	}
}