chrysopelea_mk-i.oxp
-----------------------------------------

An oxp for Oolite v1.90 and higher that adds a new ship called 'Chrysopelea MK-I'. 

Installing and Uninstalling this oxp
------------------------------------
To install, simply copy the chrysopelea_mk-i.oxp folder into your oolite AddOns folder, next time you start the game the ship will be available.
to uninstall, move or delete the chrysopelea_mk-i.oxp folder from your oolite AddOns folder, next time you start the game the ship will be gone.

Links
-----
Oolite: http://www.oolite.org/
Oolite Bulletin boards: http://www.aegidian.org/bb/

Update History:
20 October 2021 - added little script to make the Erlage system special, relesae version 2.3
20 March 2021 - adjusted texture colors, added new texture, removed green and yellow textures, adjusted shipdata, relesae version 2.2
15 February 2021 - complete update of model, textures and stats, release version 2.0
22 Februar 2020 - updated stats
6 January 2017 - texture rework and shaders by Keeper
12 Oktober 2016 - original release
  
------------------------------------
By Tsoj
This work is licensed under the Attribution-NonCommercial-ShareAlike 4.0 International license (CC BY-NC-SA 4.0). To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
